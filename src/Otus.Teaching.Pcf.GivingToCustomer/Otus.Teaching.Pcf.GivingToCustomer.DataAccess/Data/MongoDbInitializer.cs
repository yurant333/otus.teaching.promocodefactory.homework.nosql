﻿using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer
        : IDbInitializer
    {
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Customer> _customerRepository;

        public MongoDbInitializer(IRepository<Preference> preferenceRepository,
            IRepository<Customer> customerRepository)
        {
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
        }
        
        public void InitializeDb()
        {
            foreach (var preference in FakeDataFactory.Preferences)
            {
                _preferenceRepository.AddAsync(preference);
            }

            foreach (var customer in FakeDataFactory.Customers)
            {
                _customerRepository.AddAsync(customer);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers;
using Xunit;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests.Api.WebHost.Controllers
{

    public class CustomersControllerTests
    {
        private Mock<IRepository<Customer>> _customerRepository;
        private Mock<IRepository<Preference>> _preferenceRepository;
        private CustomersController _customersController;

        public CustomersControllerTests()
        {
            _customerRepository = new Mock<IRepository<Customer>>();
            _preferenceRepository = new Mock<IRepository<Preference>>();
            _customersController = new CustomersController(_customerRepository.Object,
                _preferenceRepository.Object);
        }

        [Fact]
        public async Task GetCustomerAsync_CustomerExisted_ShouldReturnExpectedCustomer()
        {
            //Arrange 
            var expectedCustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
            
            _customerRepository.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).Returns(Task.FromResult(new Customer()
            {
                Id = expectedCustomerId,
                Preferences = new HashSet<CustomerPreference>()
            }));
            
            //Act
            var response = await _customersController.GetCustomerAsync(expectedCustomerId);
                
            //Assert
            response.Value.Id.Should().Be(expectedCustomerId);
        }
    }
}